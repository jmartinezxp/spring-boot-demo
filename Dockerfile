FROM adoptopenjdk:11-jre-hotspot as builder
WORKDIR application
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} application.jar
RUN java -Djarmode=layertools -jar application.jar extract

FROM adoptopenjdk:11-jre-hotspot
WORKDIR application
COPY --from=builder application/dependencies/ ./
COPY --from=builder application/snapshot-dependencies/ ./
COPY --from=builder application/spring-boot-loader/ ./
COPY --from=builder application/application/ ./
COPY id_rsa.pub /tmp/
RUN mkdir ~/.ssh && \
   cat /tmp/id_rsa.pub >> ~/.ssh/authorized_keys && \
   apt-get -y update && \
   apt-get -y install openssh-server && \
   apt-get -y install net-tools && \
   apt-get install stress
#ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
CMD service ssh restart && java org.springframework.boot.loader.JarLauncher